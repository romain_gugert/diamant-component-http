<?php
/*
 * This file is part of the Diamant Http package.
 *
 * (c) Romain Gugert <romain.gugert@globalis-ms.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Diamant\Component\Http;

use \InvalidArgumentException;
use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\StreamInterface;

/**
 * Abstract message (base class for Request and Response)
 *
 * This class represents a general HTTP message. It provides common properties and methods for
 * the HTTP request and response, as defined in the PSR-7 MessageInterface.
 *
 */
abstract class Message implements MessageInterface
{
    /**
     * Valid protocole versions
     *
     * @var array
     */
    protected $validProtocolVersions = [
        '1.0' => true,
        '1.1' => true,
        '2.0' => true,
    ];

    /**
     * Protocol version
     *
     * @var string
     */
    protected $protocolVersion = '1.1';

    /**
     * Headers
     *
     * @var array
     */
    protected $headers= [];

    /**
     * Header normalize names
     *
     * @var array
     */
    protected $headerNames = [];

    /**
     * Body object
     *
     * @var \Psr\Http\Message\StreamInterface
     */
    protected $body;


    /**
     * Create new HTTP request.
     *
     * Adds a host header when none was provided and a host is defined in uri.
     *
     * @param StreamInterface|string|ressoure   $body               The message body object
     * @param array                             $headers            The message headers collection
     * @param string                            $protocolVersion    The message protocol version
     * @throws InvalidArgumentException
     */
    public function __construct($body = 'php://temp', array $headers = [], $protocolVersion = '1.1')
    {
        if (!is_string($body) && !is_resource($body) && !$body instanceof StreamInterface) {
            throw new InvalidArgumentException(
                'Body must be a string stream resource identifier, an actual stream resource, or a Psr\Http\Message\StreamInterface'
            );
        }
        $this->body = ($body instanceof StreamInterface) ? $body : new Stream($body, 'wb+');

        //Filter headers
        foreach ($headers as $name => $value) {
            if (!is_string($name) || $this->isValidHeaderName($name)) {
                continue;
            }
            if (is_string($value)) {
                $value = [$value];
            }
            if (!is_array($value)) {
                continue;
            }
            foreach ($value as $k => $v) {
                if (!$this->isValidHeaderValue($v)) {
                    unset($value[$k]);
                }
            }
            $this->headersNames[$this->normalize($name)];
            $this->headers[$name] = $value;
        }

        //Filter protocol version
        $this->protocolVersion = $this->isValidProtocolVersion($protocolVersion) ? $protocolVersion : '1.1';
    }

    /***************************************************************************
     * Protocol
     **************************************************************************/

    /**
     * Retrieves the HTTP protocol version as a string.
     *
     * The string MUST contain only the HTTP version number (e.g., "1.1", "1.0").
     *
     * @return string HTTP protocol version.
     */
    public function getProtocolVersion()
    {
        return $this->protocolVersion;
    }

    /**
     * Return an instance with the specified HTTP protocol version.
     *
     * The version string MUST contain only the HTTP version number (e.g.,
     * "1.1", "1.0").
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that has the
     * new protocol version.
     *
     * @param string $version HTTP protocol version
     * @return static
     * @throws InvalidArgumentException if the http version is an invalid number
     */
    public function withProtocolVersion($version)
    {
        if (!$this->isValidProtocolVersion($version)) {
            throw new InvalidArgumentException('Invalid HTTP version. Must be one of: ' . implode(', ', array_keys($this->validProtocolVersions)));
        }
        $clone = clone $this;
        $clone->protocolVersion = $version;
        return $clone;
    }

    /**
     * Is Valid Protocol version ?
     *
     * @param  string   $version
     * @return boolean
     */
    protected function isValidProtocolVersion($version)
    {
        return isset($this->validProtocolVersions[$version]);
    }

    /*******************************************************************************
     * Header
     ******************************************************************************/

    /**
     * Retrieves all message header values.
     *
     * The keys represent the header name as it will be sent over the wire, and
     * each value is an array of strings associated with the header.
     *
     *     // Represent the headers as a string
     *     foreach ($message->getHeaders() as $name => $values) {
     *         echo $name . ": " . implode(", ", $values);
     *     }
     *
     *     // Emit headers iteratively:
     *     foreach ($message->getHeaders() as $name => $values) {
     *         foreach ($values as $value) {
     *             header(sprintf('%s: %s', $name, $value), false);
     *         }
     *     }
     *
     * While header names are not case-sensitive, getHeaders() will preserve the
     * exact case in which headers were originally specified.
     *
     * @return array Returns an associative array of the message's headers. Each
     *     key MUST be a header name, and each value MUST be an array of strings
     *     for that header.
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * Checks if a header exists by the given case-insensitive name.
     *
     * @param string $name Case-insensitive header field name.
     * @return bool Returns true if any header names match the given header
     *     name using a case-insensitive string comparison. Returns false if
     *     no matching header name is found in the message.
     */
    public function hasHeader($name)
    {
        return isset($this->headerNames[$this->normalizeHeaderName($name)]);
    }

    /**
     * Retrieves a message header value by the given case-insensitive name.
     *
     * This method returns an array of all the header values of the given
     * case-insensitive header name.
     *
     * If the header does not appear in the message, this method MUST return an
     * empty array.
     *
     * @param string $name Case-insensitive header field name.
     * @return string[] An array of string values as provided for the given
     *    header. If the header does not appear in the message, this method MUST
     *    return an empty array.
     */
    public function getHeader($name)
    {
        if (!$this->hasHeader($name)) {
            return [];
        }
        $header = $this->headerNames[$this->normalizeHeaderName($name)];
        return $this->headers[$header];
    }

    /**
     * Retrieves a comma-separated string of the values for a single header.
     *
     * This method returns all of the header values of the given
     * case-insensitive header name as a string concatenated together using
     * a comma.
     *
     * NOTE: Not all header values may be appropriately represented using
     * comma concatenation. For such headers, use getHeader() instead
     * and supply your own delimiter when concatenating.
     *
     * If the header does not appear in the message, this method MUST return
     * an empty string.
     *
     * @param string $name Case-insensitive header field name.
     * @return string A string of values as provided for the given header
     *    concatenated together using a comma. If the header does not appear in
     *    the message, this method MUST return an empty string.
     */
    public function getHeaderLine($name)
    {
        return implode(',', $this->getHeader($name));
    }

    /**
     * Return an instance with the provided value replacing the specified header.
     *
     * While header names are case-insensitive, the casing of the header will
     * be preserved by this function, and returned from getHeaders().
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that has the
     * new and/or updated header and value.
     *
     * @param string $name Case-insensitive header field name.
     * @param string|string[] $value Header value(s).
     * @return self
     * @throws \InvalidArgumentException for invalid header names or values.
     */
    public function withHeader($name, $value)
    {
        if (is_string($value)) {
            $value = [$value];
        }

        if (!is_array($value) || !$this->arrayContainsOnlyStrings($value)) {
            throw new InvalidArgumentException(
                'Invalid header value; must be a string or array of strings'
            );
        }

        if (!$this->isValidHeaderName($name)) {
            throw new InvalidArgumentException('Invalid header name');
        }

        foreach ($value as $v) {
            if (!$this->isValidHeaderValue($v)) {
                throw new InvalidArgumentException('Invalid header value');
            }
        }

        $clone = clone $this;
        $clone->headerNames[$this->normalizeHeaderName($name)] = $name;
        $clone->headers[$name]         = $value;
        return $clone;
    }

    /**
     * Return an instance with the specified header appended with the given value.
     *
     * Existing values for the specified header will be maintained. The new
     * value(s) will be appended to the existing list. If the header did not
     * exist previously, it will be added.
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that has the
     * new header and/or value.
     *
     * @param string $name Case-insensitive header field name to add.
     * @param string|string[] $value Header value(s).
     * @return self
     * @throws \InvalidArgumentException for invalid header names or values.
     */
    public function withAddedHeader($name, $value)
    {
        if (is_string($value)) {
            $value = [$value];
        }

        if (!is_array($value) || !$this->arrayContainsOnlyStrings($value)) {
            throw new InvalidArgumentException(
                'Invalid header value; must be a string or array of strings'
            );
        }

        if (!$this->isValidHeaderName($name)) {
            throw new InvalidArgumentException('Invalid header name');
        }

        foreach ($value as $v) {
            if (!$this->isValidHeaderValue($v)) {
                throw new InvalidArgumentException('Invalid header value');
            }
        }

        if (!$this->hasHeader($name)) {
            return $this->withHeader($name, $value);
        }

        $name = $this->headerNames[$this->normalizeHeaderName($name)];
        $clone = clone $this;
        $clone->headers[$name] = array_merge($this->headers[$name], $value);
        return $clone;
    }

    /**
     * Return an instance without the specified header.
     *
     * Header resolution MUST be done without case-sensitivity.
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that removes
     * the named header.
     *
     * @param string $name Case-insensitive header field name to remove.
     * @return self
     */
    public function withoutHeader($name)
    {
        if (! $this->hasHeader($name)) {
            return clone $this;
        }

        $normalized = $this->normalizeHeaderName($name);
        $name   = $this->headerNames[$normalized];
        $clone = clone $this;
        unset($clone->headers[$name], $clone->headerNames[$normalized]);
        return $clone;
    }

    /**
     * Normalize header name
     *
     * @param string $name
     * @return  boolean
     */
    protected function normalizeHeaderName($name)
    {
        return strtolower($name);
    }

    /**
     * Is header name is valid ?
     *
     * @see http://tools.ietf.org/html/rfc7230#section-3.2
     * @param string $name
     * @return  boolean
     */
    protected function isValidHeaderName($name)
    {
        return preg_match('/^[0-9A-Za-z\!\#$%&\'\*\+\-\.\^_`\|\~]+$/', (string)$name);
    }

    /**
     * Is header value is valid ?
     *
     * @see http://tools.ietf.org/html/rfc7230#section-3.2
     * @param string $name
     * @return  boolean
     */
    protected function isValidHeaderValue($value)
    {
        $value  = (string) $value;
        // Look for:
        // \n not preceded by \r, OR
        // \r not followed by \n, OR
        // \r\n not followed by space or horizontal tab; these are all CRLF attacks
        if (preg_match("#(?:(?:(?<!\r)\n)|(?:\r(?!\n))|(?:\r\n(?![ \t])))#", $value)) {
            return false;
        }
        // Non-visible, non-whitespace characters
        // 9 === horizontal tab
        // 10 === line feed
        // 13 === carriage return
        // 32-126, 128-254 === visible
        // 127 === DEL (disallowed)
        // 255 === null byte (disallowed)
        if (preg_match('/[^\x09\x0a\x0d\x20-\x7E\x80-\xFE]/', $value)) {
            return false;
        }
        return true;
    }

    /*******************************************************************************
     * Body
     ******************************************************************************/

    /**
     * Gets the body of the message.
     *
     * @return Psr\Http\Message\StreamInterface Returns the body as a stream.
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Return an instance with the specified message body.
     *
     * The body MUST be a StreamInterface object.
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return a new instance that has the
     * new body stream.
     *
     * @param Psr\Http\Message\StreamInterface $body Body.
     * @return self
     * @throws \InvalidArgumentException When the body is not valid.
     */
    public function withBody(StreamInterface $body)
    {
        $clone = clone $this;
        $clone->body = $body;
        return $clone;
    }

    /***************************************************************************
     * Utility
     **************************************************************************/

    /**
     * Test that an array contains only strings
     *
     * @param array $array
     * @return bool
     */
    private function arrayContainsOnlyStrings(array $array)
    {
        foreach ($array as $item) {
            if (!is_string($item)) {
                return false;
            }
        }
        return true;
    }
}
