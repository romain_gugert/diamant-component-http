<?php
/*
 * This file is part of the Diamant Http package.
 *
 * (c) Romain Gugert <romain.gugert@globalis-ms.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Diamant\Component\Http\Tests;

use Diamant\Component\Http\Tests\MessageStub as Message;

class MessageTest extends \PHPUnit_Framework_TestCase
{
    /*******************************************************************************
     * Protocol
     ******************************************************************************/

    public function testProtocolHasAcceptableDefault()
    {
        $message = new Message();
        $this->assertEquals('1.1', $message->getProtocolVersion());
    }

    public function testProtocolMutatorReturnsCloneWithChanges()
    {
        $message = new Message();
        $cloneMessage = $message->withProtocolVersion('1.0');
        $this->assertNotSame($message, $cloneMessage);
        $this->assertEquals('1.1', $message->getProtocolVersion());
        $this->assertEquals('1.0', $cloneMessage->getProtocolVersion());
    }

    public function testWithProtocolVersionInvalidThrowsException()
    {
        $message = new Message();
        $this->setExpectedException('InvalidArgumentException');
        $message->withProtocolVersion('3.0');
    }

    /*******************************************************************************
     * Headers
     ******************************************************************************/

    public function testConstructorIgonoresInvalidHeaders()
    {
        $headers = [
            [ 'INVALID' ],
            'x-invalid-null' => null,
            'x-invalid-true' => true,
            'x-invalid-false' => false,
            'x-invalid-int' => 1,
            'x-invalid-object' => (object) ['INVALID'],
            'x-valid-string' => 'VALID',
            'x-valid-array' => [ 'VALID' ],
        ];
        $expected = [
            'x-valid-string' => [ 'VALID' ],
            'x-valid-array' => [ 'VALID' ],
        ];
        $message = new Message('php://memory', $headers);
        $this->assertEquals($expected, $message->getHeaders());
    }

    public function testGetHeaderReturnsHeaderValueAsArray()
    {
        $message = new Message();
        $cloneMessage = $message->withHeader('X-Foo', ['Foo', 'Bar']);
        $this->assertNotSame($message, $cloneMessage);
        $this->assertEquals(['Foo', 'Bar'], $message->getHeader('X-Foo'));
    }

    public function testGetHeaderLineReturnsHeaderValueAsCommaConcatenatedString()
    {
        $message = new Message();
        $cloneMessage = $message->withHeader('X-Foo', ['Foo', 'Bar']);
        $this->assertNotSame($message, $cloneMessage);
        $this->assertEquals('Foo,Bar', $message->getHeaderLine('X-Foo'));
    }

    public function testGetHeadersKeepsHeaderCaseSensitivity()
    {
        $message = new Message();
        $cloneMessage = $message->withHeader('X-Foo', ['Foo', 'Bar']);
        $this->assertNotSame($message, $cloneMessage);
        $this->assertEquals([ 'X-Foo' => [ 'Foo', 'Bar' ] ], $cloneMessage->getHeaders());
    }

    public function testGetHeadersReturnsCaseWithWhichHeaderFirstRegistered()
    {
        $message = new Message();
        $cloneMessage = $message
            ->withHeader('X-Foo', 'Foo')
            ->withAddedHeader('x-foo', 'Bar');
        $this->assertNotSame($message, $cloneMessage);
        $this->assertEquals([ 'X-Foo' => [ 'Foo', 'Bar' ] ], $cloneMessage->getHeaders());
    }

    public function testHasHeaderReturnsFalseIfHeaderIsNotPresent()
    {
        $message = new Message();
        $this->assertFalse($message->hasHeader('X-Foo'));
    }

    public function testHasHeaderReturnsTrueIfHeaderIsPresent()
    {
        $message = new Message();
        $cloneMessage = $message->withHeader('X-Foo', 'Foo');
        $this->assertNotSame($message, $cloneMessage);
        $this->assertTrue($cloneMessage->hasHeader('X-Foo'));
    }

    public function testAddHeaderAppendsToExistingHeader()
    {
        $message = new Message();
        $cloneMessage = $message->withHeader('X-Foo', 'Foo');
        $this->assertNotSame($message, $cloneMessage);
        $cloneMessage2 = $message->withAddedHeader('X-Foo', 'Bar');
        $this->assertNotSame($cloneMessage, $cloneMessage2);
        $this->assertEquals('Foo,Bar', $cloneMessage2->getHeaderLine('X-Foo'));
    }

    public function testCanRemoveHeaders()
    {
        $message = new Message();
        $cloneMessage = $message->withHeader('X-Foo', 'Foo');
        $this->assertNotSame($message, $cloneMessage);
        $this->assertTrue($cloneMessage->hasHeader('x-foo'));
        $cloneMessage2 = $cloneMessage->withoutHeader('x-foo');
        $this->assertNotSame($cloneMessage, $cloneMessage2);
        $this->assertFalse($cloneMessage2->hasHeader('X-Foo'));
    }

    public function testHeaderRemovalIsCaseInsensitive()
    {
        $message = new Message();
        $cloneMessage = $message
            ->withHeader('X-Foo', 'Foo')
            ->withAddedHeader('x-foo', 'Bar')
            ->withAddedHeader('X-FOO', 'Baz');
        $this->assertNotSame($message, $cloneMessage);
        $this->assertTrue($cloneMessage->hasHeader('x-foo'));
        $cloneMessage2 = $cloneMessage->withoutHeader('x-foo');
        $this->assertNotSame($cloneMessage, $cloneMessage2);
        $this->assertFalse($cloneMessage2->hasHeader('X-Foo'));
        $headers = $cloneMessage2->getHeaders();
        $this->assertEquals(0, count($headers));
    }

    public function testWithoutHeaderDoesNothingIfHeaderDoesNotExist()
    {
        $message = new Message();
        $this->assertFalse($message->hasHeader('X-Foo'));
        $cloneMessage = $message->withoutHeader('X-Foo');
        $this->assertNotSame($message, $cloneMessage);
        $this->assertFalse($cloneMessage->hasHeader('X-Foo'));
    }

    public function testGetHeaderReturnsAnEmptyArrayWhenHeaderDoesNotExist()
    {
        $message = new Message();
        $this->assertSame([], $message->getHeader('X-Foo-Bar'));
    }

    public function testGetHeaderLineReturnsEmptyStringWhenHeaderDoesNotExist()
    {
        $message = new Message();
        $this->assertEmpty($message->getHeaderLine('X-Foo-Bar'));
    }

    public function headersWithInjectionVectors()
    {
        return [
            'name-with-cr'           => ["X-Foo\r-Bar", 'value'],
            'name-with-lf'           => ["X-Foo\n-Bar", 'value'],
            'name-with-crlf'         => ["X-Foo\r\n-Bar", 'value'],
            'name-with-2crlf'        => ["X-Foo\r\n\r\n-Bar", 'value'],
            'value-with-cr'          => ['X-Foo-Bar', "value\rinjection"],
            'value-with-lf'          => ['X-Foo-Bar', "value\ninjection"],
            'value-with-crlf'        => ['X-Foo-Bar', "value\r\ninjection"],
            'value-with-2crlf'       => ['X-Foo-Bar', "value\r\n\r\ninjection"],
            'array-value-with-cr'    => ['X-Foo-Bar', ["value\rinjection"]],
            'array-value-with-lf'    => ['X-Foo-Bar', ["value\ninjection"]],
            'array-value-with-crlf'  => ['X-Foo-Bar', ["value\r\ninjection"]],
            'array-value-with-2crlf' => ['X-Foo-Bar', ["value\r\n\r\ninjection"]],
        ];
    }

    /**
     * @dataProvider headersWithInjectionVectors
     * @group ZF2015-04
     */
    public function testDoesNotAllowCRLFInjectionWhenCallingWithHeader($name, $value)
    {
        $message = new Message();
        $this->setExpectedException('InvalidArgumentException');
        $message->withHeader($name, $value);
    }

    /**
     * @dataProvider headersWithInjectionVectors
     * @group ZF2015-04
     */
    public function testDoesNotAllowCRLFInjectionWhenCallingWithAddedHeader($name, $value)
    {
        $message = new Message();
        $this->setExpectedException('InvalidArgumentException');
        $message->withAddedHeader($name, $value);
    }

    public function testWithHeaderAllowsHeaderContinuations()
    {
        $message = new Message();
        $cloneMessage = $message->withHeader('X-Foo-Bar', "value,\r\n second value");
        $this->assertEquals("value,\r\n second value", $cloneMessage->getHeaderLine('X-Foo-Bar'));
    }

    public function testWithAddedHeaderAllowsHeaderContinuations()
    {
        $message = new Message();
        $cloneMessage = $message->withAddedHeader('X-Foo-Bar', "value,\r\n second value");
        $this->assertEquals("value,\r\n second value", $cloneMessage->getHeaderLine('X-Foo-Bar'));
    }

    /*******************************************************************************
     * Body
     ******************************************************************************/

    public function testConstructorRaisesExceptionForInvalidStream()
    {
        $this->setExpectedException('InvalidArgumentException');
        new Message(['TOTALLY INVALID']);
    }

    public function testDefaultStreamIsWritable()
    {
        $message = new Message();
        $message->getBody()->write("test");

        $this->assertSame("test", (string)$message->getBody());
    }

    public function testGetBody()
    {
        $body = $this->getMock('Psr\Http\Message\StreamInterface');
        $message = new Message($body);
        $this->assertSame($body, $message->getBody());
    }

    public function testBodyMutatorReturnsCloneWithChanges()
    {
        $stream  = $this->getMock('Psr\Http\Message\StreamInterface');
        $message = new Message();
        $cloneMessage = $message->withBody($stream);
        $this->assertNotSame($message, $cloneMessage);
        $this->assertSame($stream, $cloneMessage->getBody());
    }
}
