<?php
/*
 * This file is part of the Diamant Http package.
 *
 * (c) Romain Gugert <romain.gugert@globalis-ms.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Diamant\Component\Http\Tests;

use Diamant\Component\Http\Message;
use Diamant\Component\Http\Headers;

/**
 * Mock object for Diamant\Component\Http\MessageTest
 */
class MessageStub extends Message
{

}
